FROM archlinux:base AS myarch

RUN pacman --noconfirm -Syu

# For paccache
RUN pacman --noconfirm -S pacman-contrib && paccache -r

SHELL ["bash", "-c"]



FROM myarch AS builder

RUN pacman --noconfirm -S base-devel git

RUN echo -e '#!/bin/bash\nyay \
  --noconfirm \
  --removemake \
  --nodiffmenu \
  --nocleanmenu \
  --noeditmenu \
  --noupgrademenu "$@"' >> /usr/local/bin/yays && chmod a+x /usr/local/bin/yays

RUN echo '%wheel ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel

RUN useradd build --create-home --gid wheel
USER build

WORKDIR /home/build/

# Install yay
RUN mkdir src && \
      cd src && \
      git clone https://aur.archlinux.org/yay-bin.git && \
      cd yay-bin && \
      makepkg -si --noconfirm

RUN yays -S ipfs-cluster-bin



FROM myarch

ARG ipfsGateway
ARG clusterName
ARG templateUrl
ARG ipfsHttpNodeMultiAddress

RUN test -n "$ipfsGateway" && test -n "$clusterName" && test -n "$templateUrl"

ENV IPFS_GATEWAY $ipfsGateway
ENV MY_CLUSTER_NAME $clusterName
ENV CLUSTER_IPFSHTTP_NODEMULTIADDRESS $ipfsHttpNodeMultiAddress

WORKDIR /root

RUN mkdir /root/aur-packages
COPY --from=builder /home/build/.cache/yay/*/*.pkg* /root/aur-packages
RUN pacman --noconfirm -U /root/aur-packages/*.pkg* && rm -rf /root/aur-packages

RUN useradd ipfs --create-home
USER ipfs
WORKDIR /home/ipfs

RUN ipfs-cluster-follow "$clusterName" init "$templateUrl"

USER root

# Remove no longer needed packages
# Remove orphans (even optional dependency orphans)
RUN paccache -r && pacman --noconfirm -R pacman-contrib && \
pacman --noconfirm -Qttdq | pacman --noconfirm -Rns -

USER ipfs

# Cluster swarm: is used by the Cluster swarm and protected by the shared secret.
# It is OK to explose this port (the cluster secret acts as a password to interact with it).
EXPOSE 9096

# HTTP API: can be exposed when enabling SSL and setting up basic authentication.
# See https://cluster.ipfs.io/documentation/reference/configuration/#restapi
EXPOSE 9094

CMD echo "IPFS_GATEWAY: $IPFS_GATEWAY" && \
  ipfs-cluster-follow "$MY_CLUSTER_NAME" run
